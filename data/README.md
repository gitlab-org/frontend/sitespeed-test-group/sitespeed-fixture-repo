# data/

This folder contains a copy of the `www-gitlab-com` repo data as of: [`a518cd81d (2021-03-02T08:54:23Z)`](https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/a518cd81d285895770ec93448a756ba0456280a2)
