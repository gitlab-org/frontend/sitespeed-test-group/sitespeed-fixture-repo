This repo contains directories and files to be used for testing by our Sitespeed setup: https://gitlab.com/gitlab-org/frontend/sitespeed-measurement-setup

## data/

Contains a copy of the `www-gitlab-com` repo data as of: [`a518cd81d (2021-03-02T08:54:23Z)`](https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/a518cd81d285895770ec93448a756ba0456280a2).

## large_directory/

Contains a large amount of directories.

## large_directory/dir_1000/

Contains two large source files.